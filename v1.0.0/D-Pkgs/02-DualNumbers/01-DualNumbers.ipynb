{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img alt=\"Julia Language Logo\" src=\"https://docs.julialang.org/en/v1/assets/logo.png\" width=\"96\" align=\"left\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# DualNumbers.jl — Automatic Differentiation\n",
    "\n",
    "[Project Page](https://github.com/JuliaDiff/DualNumbers.jl)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-dbl_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/landscape-double-sun-rock-wall-1112911/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `DualNumbers` package provides a **new number type** that **automatically** performs **differentiation** to **machine precision**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "using DualNumbers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Structure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dual numbers are a simple struct of two numerical values of the same type. From the source code:\n",
    "\n",
    "> **`https://github.com/JuliaDiff/DualNumbers.jl/blob/master/src/dual.jl`:**\n",
    "> \n",
    "> ```julia\n",
    "> const ReComp = Union{Real,Complex}\n",
    "> \n",
    "> struct Dual{T<:ReComp} <: Number\n",
    ">     value::T\n",
    ">     epsilon::T\n",
    "> end\n",
    "> ```\n",
    "\n",
    "With constructors:\n",
    "\n",
    "> ```julia\n",
    "> Dual(x::S, y::T) where {S<:ReComp,T<:ReComp} = Dual(promote(x,y)...)\n",
    "> Dual(x::ReComp) = Dual(x, zero(x))\n",
    "> ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `value` field is the number value, and the `epsilon` is the differential."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Usage"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Instantiation of Dual numbers:\n",
    "x = Dual(1.0, 1.0)\n",
    "@show x\n",
    "@show typeof(x)\n",
    "y = Dual(2.0, 1.0)\n",
    "@show y\n",
    "@show typeof(y);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a function that knows nothing about dual numbers:\n",
    "function f(x)\n",
    "    return x ^ 3\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute the function with a dual number argument:\n",
    "z = f(y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Access the fields directly to get the value and the differential\n",
    "println(\"Function: f(x) = x³\")\n",
    "println(\" f($(y.value)) = $(y.value)³    = $(z.value)\")\n",
    "println(\"f'($(y.value)) = 3($(y.value))² = $(z.epsilon)\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a Numeric function that also knows nothing about dual numbers:\n",
    "function g(x::T where T<:Number)\n",
    "    return log(x)\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute the function with a dual number argument:\n",
    "w = g(y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use `realpart` and `dualpart` functions to get the value and the differential\n",
    "println(\"Function: g(x) = ln(x)\")\n",
    "println(\" g($(y.value)) = ln($(realpart(y))) = $(realpart(w))\")\n",
    "println(\"g'($(y.value)) = 1/($(realpart(y))) = $(dualpart(w))\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Square-root function using the Babylonian (Babel: בבל) method\n",
    "function בבל(x)\n",
    "    z = x\n",
    "    while abs(z*z - x) > 1e-14\n",
    "        z = z - (z*z - x)/(2z)\n",
    "    end\n",
    "    return z\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute the function with a dual number argument:\n",
    "v = בבל(y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use `realpart` and `dualpart` functions to get the value and the differential\n",
    "println(\"Function: בבל‎(x) = √x\")\n",
    "println(\" בבל‎($(y.value)) = √$(realpart(y)) = $(realpart(v))\")\n",
    "println(\"בבל‎'($(y.value)) = 1/(2√$(realpart(y))) = $(dualpart(v))\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.0.0",
   "language": "julia",
   "name": "julia-1.0"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.0.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
