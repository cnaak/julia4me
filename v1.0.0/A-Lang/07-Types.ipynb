{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img alt=\"Julia Language Logo\" src=\"https://docs.julialang.org/en/v1/assets/logo.png\" width=\"96\" align=\"left\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This material closely follows the [Julia Language Documentation](https://docs.julialang.org/en/v1/) for the `Version 1.0.0` of `julia`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-pcb_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/board-conductors-circuits-540253/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The reader is directed to the [Types](https://docs.julialang.org/en/v1/manual/types/) Section of the Julia Documentation for an official introduction on Julia types.\n",
    "\n",
    "In cruder terms, Julia lets you program without using any explicit type declaration (as most of these presentations have been so far); however, programs can benefit from type declarations for runtime speed, data structure memory layout, and even programmer error catching."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Type Annotations — Assertions and Declarations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Julia, the `::` operator is used to annotate type information, which in turns can be used:\n",
    "\n",
    "- To provide additional type information to the compiler, which can, in some cases, make performance improvements;\n",
    "- To assert that your program works the way you expect, i.e., computed and passed around values are of the expected types."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If applied to an expression computing a value, the `::` operation works as an **type assertion**:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Checks (asserts) whether a result is any type of Float, i.e., and AbstractFloat\n",
    "(\"Hello, \" * \"World\")::AbstractFloat"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Checks whether the result is an Integer: the value is silently passed on in the affirmative case:\n",
    "(0x1b + 1_000_000)::Int"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before proceeding, let's re-define our @typeVal that prints the type of the argument and returns its value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# For now, think of macros as kind of functions that run on compile time, rather than at runtime\n",
    "macro typeVal(x)\n",
    "    return quote              # The return value is an expression that is evaluated\n",
    "        local v = $x          # The value of x\n",
    "        local t = typeof(v)   # The type of the value\n",
    "        println(\"$(t): $(v)\") # Prints \"TYPE: VALUE\"\n",
    "        v                     # The value is returned\n",
    "    end\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "According to the [Julia Language Documentation](https://docs.julialang.org/en/v1/manual/types/#Type-Declarations-1):\n",
    "\n",
    "> When appended to a variable on the left-hand side of an assignment, or as part of a local declaration, the `::` operator means something a bit different: it declares the variable to always have the specified type, like a type declaration in a statically-typed language such as `C`. Every value assigned to the variable will be converted to the declared type using `convert()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Function that returns UInt8 of the input value\n",
    "function foo(value)\n",
    "    x::UInt8 = value\n",
    "    println(\"$(summary(x)): $(repr(x))\")\n",
    "    x\n",
    "end\n",
    "\n",
    "@typeVal foo(100), foo(200), foo(250.0);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Function that returns UInt16 of the UInt8 of the input value\n",
    "function foo(value)::UInt16\n",
    "    x::UInt8 = value\n",
    "    println(\"$(summary(x)): $(repr(x))\")\n",
    "    x + 2  # Now the conversion happens on this line since it is the return value\n",
    "end\n",
    "\n",
    "@typeVal foo(100), foo(200), foo(250.0);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Abstract Types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-abs_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/background-abstract-line-2462433/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Julia, abstract types cannot be instantiated (there can be no value of an abstract type), so all values must be concrete types. Moreover, concrete types cannot have subtypes (no type can be a child of a concrete type); therefore, in the Julia type system, abstract types are used from the \"root type\" all the way to the smaller branches, while concrete ones are the leaves."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Abstract types are declared as follows:\n",
    "\n",
    "```julia\n",
    "abstract type «name» end\n",
    "abstract type «name» <: «supertype» end\n",
    "```\n",
    "\n",
    "Either of these creates a new abstract type named \"name\"; the first syntax places the newly created type under the root node—the type `Any`—while the second one places the newly created type under the already existing type named \"supertype\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's consider some of the abstract types that make up Julia's numerical hierarchy:\n",
    "\n",
    "```julia\n",
    "abstract type Number end\n",
    "abstract type Real     <: Number end\n",
    "abstract type AbstractFloat <: Real end\n",
    "abstract type Integer  <: Real end\n",
    "abstract type Signed   <: Integer end\n",
    "abstract type Unsigned <: Integer end\n",
    "```\n",
    "\n",
    "So `Number` is under `Any` and above `Real`, which is above `AbstractFloat`—so that anything below \"AbstractFloat\" is a floating point kind, which is also \"Real\", which is also a \"Number\", which is also \"something\". Moerover, either `Signed` or `Unsigned` are \"Integers\", which also are \"Real\", end so on..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `<:` operator in general means \"subtype of\", and, used in declarations like the ones above, declares the right-hand type to be an immediate supertype of the newly declared type.\n",
    "\n",
    "The `<:` operator can be used in assertions as well. `A <: B` returns `true` if and only if type `A` is subtype of type `B`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Perform some subtype tests: note that <: returns true even between distant nodes in the type tree\n",
    "Signed <: Number, Unsigned <: Integer <: Real <: Number <: Any"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An important use of abstract types is to provide default implementations for concrete types. Consider the following function, `egcd(a, b)`, which computes the greatest common divisor for inputs `a` and `b`, respectively, using the Euclid's algorithm—which only makes sense for integer inputs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Euclid's GCD algorithm:\n",
    "function egcd(a, b)\n",
    "    a, b = max(abs(a), abs(b)), min(abs(a), abs(b))\n",
    "    b == 0 ? a : egcd(b, mod(a, b))\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The **function** definition above is equivalent to a `egcd(Any, Any)` template. When `egcd` is called with defined input argument types, Julia searches for the most specific \"version\" (**method**) of `egcd` for the input types.\n",
    "\n",
    "Assuming no **method** more specific than the above is found, Julia next internally defines and compiles a **method** called `egcd` specifically for the two argument types passed, based on the generic function definition (template) above.\n",
    "\n",
    "Let's say the argument types are `Int64` and `Int64`, then Julia implicitly defines and compiles a\n",
    "\n",
    "```julia\n",
    "function egcd(a::Int64, b::Int64)\n",
    "    a, b = max(abs(a), abs(b)), min(abs(a), abs(b))\n",
    "    b == 0 ? a : eugcd(b, mod(a, b))\n",
    "end\n",
    "```\n",
    "\n",
    "and calls this **method** with the supplied `Int64` values passed to it:\n",
    "\n",
    "(The language hability of allowing for multiple definitions (**methods**) of the same named **function** based on ints input types is a central feature of Julia, that is technically called **multiple dispatch**.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@which egcd(1000, 2000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since Julia has multiple dispatch and a powerful type system, it is possible to implement the Euclidean GCD functions above in a smarter way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Euclid's GCD algorithm for Integer inputs:\n",
    "function egcd(a::Integer, b::Integer)\n",
    "    a, b = max(abs(a), abs(b)), min(abs(a), abs(b))\n",
    "    b == 0 ? a : egcd(b, mod(a, b))\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the `egcd` **function** now has 2 **methods**: the `Integer` arguments one, and the `Any` (generic) arguments one. The macro `@which` tells which method of a function is being called:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@which egcd(1000, -2000)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@which egcd(1000.0, -2000.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Primitive Types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-bit_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/binary-code-binary-binary-system-475664/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the [Julia Language Documentation](https://docs.julialang.org/en/v1/manual/types/#Primitive-Types-1):\n",
    "\n",
    "> A primitive type is a concrete type whose data consists of plain old bits. Classic examples of primitive types are integers and floating-point values. Unlike most languages, Julia lets you declare your own primitive types, rather than providing only a fixed set of built-in ones. In fact, the standard primitive types are all defined in the language itself:\n",
    ">\n",
    "> ```julia\n",
    "> primitive type Float16 <: AbstractFloat 16 end\n",
    "> primitive type Float32 <: AbstractFloat 32 end\n",
    "> primitive type Float64 <: AbstractFloat 64 end\n",
    "> ...\n",
    "> ```\n",
    ">\n",
    "> The general syntaxes for declaring a primitive type are:\n",
    ">\n",
    "> ```julia\n",
    "> primitive type «name» «bits» end\n",
    "> primitive type «name» <: «supertype» «bits» end\n",
    "> ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Composite Types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Julia composite types are **concrete** data types, i.e., ones that can be _instantiated_."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-dat_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/ball-about-binary-ball-hand-keep-457334/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the [Julia Language Documentation](https://docs.julialang.org/en/v1/manual/types/#Primitive-Types-1), with emphasis from me:\n",
    "\n",
    "> Composite types are called records, structs, or objects in various languages. A composite type is a collection of named fields, an instance of which can be treated as a single value. \n",
    "> \n",
    "> ...\n",
    "> \n",
    "> In mainstream object oriented languages, such as `C++`, `Java`, `Python` and `Ruby`, composite types also have named functions associated with them, and the combination is called an \"object\". In purer object-oriented languages, such as `Ruby` or `Smalltalk`, all values are objects whether they are composites or not. In less pure object oriented languages, including `C++` and `Java`, some values, such as integers and floating-point values, are not objects, while instances of user-defined composite types are true objects with associated methods. In `Julia`, **all values are objects, but functions are not bundled with the objects they operate on**.\n",
    "\n",
    "That is due to `Julia` being a multiple dispatch language, in which function methods are chosen based on the types of **all** parameters—not just the first one.\n",
    "\n",
    "> Thus, it would be inappropriate for functions to \"belong\" to only their first argument. Organizing methods into function objects rather than having named bags of methods \"inside\" each object ends up being a highly beneficial aspect of the language design."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Julia composite types are delared with the `struct` keyword:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct linkDot\n",
    "    seq::UInt64\n",
    "    x::Float64\n",
    "    y::Float64\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`struct` objects are instatiated with a _constructor_ of the same name:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = linkDot(1, 0.0, 0.0);\n",
    "B = linkDot(2, 1.0, 1.0);\n",
    "C = linkDot(3, 2.0, 0.0);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal A; @typeVal B; @typeVal C;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Existing fields can be examined with some `field...` functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fieldnames(linkDot), fieldcount(linkDot)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "[\n",
    "    (fieldoffset(linkDot, i), fieldname(linkDot, i), fieldtype(linkDot, i))\n",
    "    for i in 1:fieldcount(linkDot)\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For reasons of code speed and memory efficiency, Julia composite type object instances are **immutable** by default:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A.x = 10.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To generate **mutable** objects, use the `mutable` keyword:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mutable struct lnkDot\n",
    "    seq::UInt64\n",
    "    x::Float64\n",
    "    y::Float64\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "D = lnkDot(4, 1.0, -1.0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "D.x = 0.75\n",
    "D"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the [Julia Language Documentation](https://docs.julialang.org/en/v1/manual/types/#Mutable-Composite-Types-1), with emphasis from me:\n",
    "\n",
    "> In order to support mutation, such objects are generally **allocated on the heap**, and have **stable memory addresses**. \\[...\\] In deciding whether to make a type mutable, ask whether two instances with the same field values would be considered identical, or if they might need to change independently over time. If they would be considered identical, the type should probably be immutable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Example: \"water\" at given temperature and specific volume always represent a substance at a given state, and multiple such instances are (macroscopically) indistinguishable, thus:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct state\n",
    "    fl::String\n",
    "    T::Float64  # [K]\n",
    "    v::Float64  # [m³/kg]\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "st1 = state(\"water\", 300.0, 0.998)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "st2 = state(\"water\", 300.0, 0.998)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "st1 == st2, st1 === st2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Type Unions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A type union is a special abstract type that can hold either of its types. It is constructed with the `Union` keyword:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "IntOrString = Union{Int, String}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Since IntOrString is either type, the following will succeed:\n",
    "42::IntOrString, \"Fourty two\"::IntOrString"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the [Julia Language Documentation](https://docs.julialang.org/en/v1/manual/types/#Type-Unions-1):\n",
    "\n",
    "> A particularly useful case of a `Union` type is `Union{T, Nothing}`, where `T` can be any type and `Nothing` is the singleton type whose only instance is the object `nothing`. This pattern is the Julia equivalent of `Nullable`, `Option` or `Maybe` types in other languages. Declaring a function argument or a field as `Union{T, Nothing}` allows setting it either to a value of type `T`, or to `nothing` to indicate that there is no value."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6. Parametric Types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In simple terms, parametric types in Julia are types that accepts parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Point{T}\n",
    "    x::T\n",
    "    y::T\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Thus:\n",
    "Point{Int64} <: Point, Point{Float64} <: Point"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# However:\n",
    "Point{Int64} <: Point{Int}, Point{Float64} <: Point{AbstractFloat}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the line above, the `<:` operator returns `true` only if the memory layout is the same for the data types:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Point{Int32} <: Point{Int}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal Point(1.0, -3.0);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following method becomes a template for all real types of point:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function norm(p::Point{<:Real})\n",
    "    sqrt(p.x^2 + p.y^2)\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "norm(Point(3.0, 4.0))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "norm(Point(3, 4))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@which norm(Point(3, 4))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# For ambiguous type parameter {T}:\n",
    "Point(1,2.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 6.1 Tuples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tuples are parametric types that are constructed with `(val, [...])`, and represent (immutable) function arguments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal (1, \"2\", 3.0);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tuple items are accessible by indices, since tuples do not have field names:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "myT = (1, \"2\", 3.0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "myT[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 6.2 Named Tuples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Named tuples have field names:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "typeof((x = 1, y = 2.0, z = \"three\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "myNT = NamedTuple{(:x, :y, :z), Tuple{Int64, Float64, String}}((1, 2.0, \"three\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "myNT.y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 7. UnionAll Types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A union of types for all values of a type parameter is called a `UnionAll` type, and is declared with the `where` keyword:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "RealTuple = Tuple{T} where T <: Real"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aa = RealTuple{Int64}((1, ))\n",
    "@typeVal aa;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The type of all 1-dimensional arrays:\n",
    "Array{T,1} where T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The type of all integer matrices:\n",
    "Array{T, 2} where T <: Integer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "M = [ [1  2];\n",
    "      [3  4] ]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "typeof(M) <: Array{T, 2} where T <: Integer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = [ [0x1  0x2];\n",
    "      [0x3  0x4] ]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "typeof(N) <: Array{T, 2} where T <: Integer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "IntMatrix = Array{T, 2} where T <: Integer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 8. Operations on Types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `isa(obj, type)`, which returns `true` if `typeof(obj) == type`, etc., is useful to perform inline type tests:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-ten_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/tennis-tennis-ball-spinning-ball-1381230/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "isa(M, IntMatrix), isa(N, IntMatrix)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the following examples, taken from the [Julia Language Documentation](https://docs.julialang.org/en/v1/manual/types/#UnionAll-Types-1):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const T1 = Array{Array{T,1} where T, 1};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const T2 = Array{Array{T,1}, 1} where T;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "myA = [ [1, 2, 3], [1.0, 2.0], [\"a\", \"b\"] ]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "myB = [ [1, 2, 3], [4, 5], [6, 7] ]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "isa(myA, T1), isa(myB, T1), isa(myA, T2), isa(myB, T2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "supertype(Int64)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function typeParents(T::DataType)\n",
    "    println(T)\n",
    "    while T != Any\n",
    "        T = supertype(T)\n",
    "        println(T)\n",
    "    end\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "typeParents(Int64)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "typeParents(typeof(\"test\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "typeParents(typeof('a'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subtypes(AbstractString)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 9. Value Types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Julia allows one to have value types that can be used to dispatch based on values. The construction goes by defining a parametric type that accepts the value:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-mon_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/monitor-binary-binary-system-1307227/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The fieldless (singleton) parametric type definition:\n",
    "struct Flag{B}\n",
    "end\n",
    "\n",
    "# The constructor with a value parameter\n",
    "Flag(B) = Flag{B}()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Example of instantiations\n",
    "Flag(true), Flag(false)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Dispatch by value:\n",
    "myFunc(safe::Flag{true}) = \"Safe mode\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Dispatch by value:\n",
    "myFunc(safe::Flag{false}) = \"Unsafe mode\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "myFunc(Flag(true))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "myFunc(Flag(false))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Version Information"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"./fig/03-juliaREPL-versionInfo.png\" alt=\"Version Information\" width=\"892\">"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.0.0",
   "language": "julia",
   "name": "julia-1.0"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.0.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
