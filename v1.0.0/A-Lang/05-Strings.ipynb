{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img alt=\"Julia Language Logo\" src=\"https://docs.julialang.org/en/v1/assets/logo.png\" width=\"96\" align=\"left\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Strings"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This material closely follows the [Julia Language Documentation](https://docs.julialang.org/en/v1/) for the `Version 1.0.0` of `julia`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-str_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/acoustic-guitar-guitar-336479/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Engineering and scientific computing frequently involves dealing with strings (of characters) for reporting purposes, and for some data representation, and even as a data structure in some algorithms. Thus, it's important to know them.\n",
    "\n",
    "Julia makes distiction between a single `character` type and a `string` type, even if the `string` is a one-character long. This distinction influences the syntax. If you are comming to `julia` from `Python`, this will be noticeable! (...and at the same time, fairly quick to adjust)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Characters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each character, or `Char`, represents a **single letter**:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"https://upload.wikimedia.org/wikipedia/commons/0/0a/Digital_rain_animation_medium_letters_clear.gif\" width=\"360\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://en.wikipedia.org/wiki/Matrix_digital_rain\">en.wikipedia.org</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Julia, a character (a `Char`) is just a `32` bit primitive type. It has an appropriate **character representation** but is able to do some **arithmetic**, such as adding and subtracting integers (as to shift the given character to higher or lower values). A `Char` may also be converted numeric value representing a Unicode code point."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before proceeding, let's re-define our `@typeVal` macro that prints the type of the argument and returns its value, as to better \"see\" what's happening:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# For now, think of macros as kind of functions that run on compile time, rather than at runtime\n",
    "macro typeVal(x)\n",
    "    return quote              # The return value is an expression that is evaluated\n",
    "        local v = $x          # The value of x\n",
    "        local t = typeof(v)   # The type of the value\n",
    "        println(\"$(t): $(v)\") # Prints \"TYPE: VALUE\"\n",
    "        v                     # The value is returned\n",
    "    end\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A literal `Char` is produced with the `''` delimiters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal '光'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the previous input, we've hadn't end the line with a semicolon, as to demonstrate how Julia prints a `Char`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal UInt32('\\U5149')\n",
    "@typeVal UInt32('光')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal Char(0x5149)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some `Char` arithmetic:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal 'A' + 2; @typeVal 'A' + 32; @typeVal 'a' - 'A'; 'A' < 'a'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Strings"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`Strings` are text elements, usually represented by a sequence of characters:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/640px-Book_of_John.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://commons.wikimedia.org/wiki/File:Book_of_John.jpg\">commons.wikimedia.org</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "String literals are delimited by double quotes (`\"...\"`) or triple double quotes (`\"\"\"...\"\"\"`). As said above, `String`'s **are not** `Char`'s:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal \"a\"; @typeVal \"\"\"a\"\"\"; (\"a\" == 'a', \"\"\"a\"\"\" == 'a', \"a\"[1] == 'a', \"a\" === \"\"\"a\"\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "String literals with escapes: printing and displaying:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "st = (\n",
    "    \"\"\"with \"quotes\" inside!\"\"\",   # No need to escape (\") inside triple quotes\n",
    "    \"\\\"quote\\\" in the beginning\",  # Escaped (\") at the beginning\n",
    "    \"with final \\\"quotes\\\"\",       # Escaped (\") at the end\n",
    "    \"with final slash: \\\\\",        # Escaped (\\) at the end\n",
    "    \"escaped quote literal: \\\\\\\"\", # Special case to produce a literal (\\\")\n",
    ")\n",
    "for i in st\n",
    "    println(i)\n",
    "end\n",
    "st"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1 Looping over Strings\n",
    "\n",
    "Roughly speaking, Julia string indexing goes byte by byte, while some single string characters may take a different amount of bytes (since Julia strings are UTF-8 encoded). Thus, one needs special functions to correctly loop over string characters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "houses = \"gr: (οικία); he: (בית); ru: (дом);\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Houses has 34 characters, but takes 45 bytes!\n",
    "# Its first and last indices are 1 and 45!\n",
    "length(houses), sizeof(houses), firstindex(houses), lastindex(houses), ncodeunits(houses)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Indices can be integers, integer ranges, or the \"end\" keyword:\n",
    "houses[1], houses[6], houses[1:6], houses[end]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Indicing strings on positions that result in an UTF-8 error, the error is thrown:\n",
    "houses[7]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may be wondering what is the next **valid** index after 6... the function `nextindex(str, i, n=1)` returns exactly that:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# What is the next valids index after 6?\n",
    "nextind(houses, 6), nextind(houses, 6, 2) # Here, n = 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Therefore we see that in the greek portion, each character takes up two bytes!\n",
    "houses[6:10], length(houses[6:10]), sizeof(houses[6:10]), UInt16(houses[6]), bitstring(UInt16(houses[6]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we are to **manually** generate an iterable with _all_ valid indices of `houses`, we could write something like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "`function vInd(str)`\n",
    "\n",
    "Returns an array of all valid indices of `str`.\n",
    "\"\"\"\n",
    "function vInd(str)\n",
    "    i, j = firstindex(str), lastindex(str)\n",
    "    vi = Int64[] # This is more specific than v = []\n",
    "    while i < j\n",
    "        append!(vi, i)\n",
    "        i = nextind(str, i)\n",
    "    end\n",
    "    append!(vi, i)\n",
    "    return vi\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "?vInd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "println(\"Valid houses indices: \", vInd(houses))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, `Strings` themselves are iterable, so one can do a very simple loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# However an easier way of looping through a string would be:\n",
    "for c in houses\n",
    "    print(\"\\\"$c\\\", \")\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If character index information is needed in the loop, one can conveniently use `enumerate`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The easiest way of looping through a string while having character and index information is:\n",
    "for c in enumerate(houses)\n",
    "    d = (c[1], nextind(houses, 0, c[1]), c[2])\n",
    "    print(\"\\\"$d\\\", \")\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `ncodeunits()` returns the amount of **code units** (which is 8 bits for UTF-8) in a string, thus telling how \"large\" a character representation is in the underlying encoding:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Latin characters \"ab\" are one byte wide each\n",
    "# Greek characters \"αβ\" are two bytes wide each\n",
    "# The CJK Unified Ideographs \"月光\" (U+6708, U+5149) are three bytes wide each\n",
    "# The Domino Tiles \"🂐🂑\" (U+1F090, U+1F091) are 4 bytes wide each\n",
    "\n",
    "for i in \"ab.αβ.月光.🂐🂑\"\n",
    "    print((ncodeunits(string(i)), i))\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Also, `enumerate` can be used to generate valid index information in a much more convenient way, with an array comprehension. Comprehension syntax is similar to set construction notation in mathematics, which look familiar for those comming from `Python`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Valid houses indices: \", [ nextind(houses, 0, i) for i in 1:length(houses) ])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2 String Concatenation\n",
    "\n",
    "In Julia, the `string()` function concatenates `Strings` (with the `print()` function).\n",
    "\n",
    "The `*` operator on `Strings` concatenates them:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-cat_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/rope-sea-barcelona-port-haven-1314964/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "string(\n",
    "    \"Hello, \",\n",
    "    \"there!\"\n",
    "), \"Hello, \" * \"there!\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For those comming from `Python`, for instance, it may look very strange that `*` concatenates (instead of multiplying copies of) `String`s. From the [Julia Documentation](https://docs.julialang.org/en/v1/manual/strings/#Concatenation-1), we read:\n",
    "\n",
    "> \\[...\\] this use of `*` has precedent in mathematics, particularly in abstract algebra.\n",
    ">\n",
    "> In mathematics, `+` usually denotes a _commutative_ operation, where the order of the operands does not matter. An example of this is matrix addition, where `A + B == B + A` for any matrices `A` and `B` that have the same shape. In contrast, `*` typically denotes a _noncommutative_ operation, where the order of the operands does matter. An example of this is matrix multiplication, where in general `A * B != B * A`. As with matrix multiplication, string concatenation is noncommutative: `greet * whom != whom * greet`. As such, `*` is a more natural choice for an infix string concatenation operator, consistent with common mathematical use.\n",
    "\n",
    "In order to multiply copies of a `String` in Julia, one uses the `^` operator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The following prints a 72-character-long line of text:\n",
    "print(\"-\" ^ 72)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3 String \"Interpolation\"\n",
    "\n",
    "The so-called string interpolation in Julia is the hability of expading (replacing) a literal `\"$expression\"` by the value of `expression`. This has been used in the above examples; however, only for string expansion. The examples below are more generic:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Addition expression interpolation\n",
    "a, b = 1, 2\n",
    "print(\"a + b = $(a+b)\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Direct substitution, and (recursive) function call interpolation\n",
    "n = 20\n",
    "rFib(N) = N < 2 ? N : rFib(N-1) + rFib(N-2)\n",
    "print(\"F_$n = $(rFib(n))\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Array interpolation\n",
    "v = vInd(houses)\n",
    "println(\"Valid houses indices: $([ i[1] for i in enumerate(houses) ]).\")\n",
    "println(\"Valid houses indices: $v.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3 String Operations\n",
    "\n",
    "Some common string operation follows:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-mag_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/phrases-magnified-dictionary-text-390805/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = \"gr: (οικία)\"; b = \"he: (בית)\"; c = \"ru: (дом)\";\n",
    "a, b, c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Comparisons:\n",
    "a < b, b != c, c == a, isequal(a[2], c[1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Search: findfirst with a pattern string returns a range / with a test function, returns an index:\n",
    "findfirst(\"α)\", a), findfirst(isequal('α'), a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@which findfirst(\"α)\", a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@which findfirst(isequal('α'), a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Findnext accepts an offset argument:\n",
    "langs = \"γλωσσολαλια\"\n",
    "i = findfirst(isequal('λ'), langs)              # Finds the first lambda in \"γλωσσολαλια\"\n",
    "j = findnext(\"λ\", langs, nextind(langs, i))     # Finds the next lambda in \"γλωσσολαλια\"\n",
    "k = findnext(\"λ\", langs, nextind(langs, j[1]))  # Finds the next lambda in \"γλωσσολαλια\"\n",
    "i, j, k, langs[i], langs[j], langs[k]           # Note that String[range] returns a (sub)-String"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Test for occurence:\n",
    "occursin(\"Julia\", \"JuliaLang is pretty cool!\"), # Literal search pattern\n",
    "occursin(r\"a.a\", \"aba\"),   # RegExp search pattern – note the preceeding r\"...\"\n",
    "occursin(r\"a.a\", \"abba\")   # RegExp search pattern – note the preceeding r\"...\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Repeating and joining\n",
    "println(repeat(\"\\\\o/ \", 5))\n",
    "println(\n",
    "    join(\n",
    "        [\"first\", \"second\", \"third\"],\n",
    "        \", \",\n",
    "        \" and \"\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"\\\\o/\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.4 Non-Standard String Literals\n",
    "\n",
    "These include:\n",
    "\n",
    "- Regular Expressions;\n",
    "- Byte-Arrays;\n",
    "- Version numbers; and\n",
    "- Raw Strings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Regular Expression: r\"...\"\n",
    "chem = r\"\\b(?<num>\\d+)?(?<mol>(?<piece>[A-Z][a-z]?(?<cnt>\\d+)?)+)\\b\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function matchAll(regx, text)\n",
    "    md = Dict()\n",
    "    i = 'α'\n",
    "    m = match(regx, text)\n",
    "    while !(m === nothing)\n",
    "        md[i] = m\n",
    "        m = match(regx, text, md[i].offset + length(md[i].match))\n",
    "        i += 1\n",
    "    end\n",
    "    return md\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "matchAll(chem, \"Consider the reaction: 2C8H18 + 25O2 → 16CO2 + 18H2O\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Version numbers: major, minor, patch, pre-release, annotations; with comparison\n",
    "v\"1\", v\"0.2\", v\"0.1-alpha+x64\", v\"0.2\" < v\"0.2.1\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Raw strings:\n",
    "raw\"No need for escaping \\o/!! $literal\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Version Information"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"./fig/03-juliaREPL-versionInfo.png\" alt=\"Version Information\" width=\"892\">"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.0.0",
   "language": "julia",
   "name": "julia-1.0"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.0.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
