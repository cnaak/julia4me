{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img alt=\"Julia Language Logo\" src=\"https://docs.julialang.org/en/v1/assets/logo.png\" width=\"96\" align=\"left\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This material closely follows the [Julia Language Documentation](https://docs.julialang.org/en/v1/) for the `Version 1.0.0` of `julia`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-rst_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/gears-metal-rust-technology-1666499/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Engineering problems can frequently be solved by programs written in the **functional paradigm**, in which units of code (usually re-usable ones) are placed inside `function`s.\n",
    "\n",
    "In this notebook, we'll get to know Julia support for functions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Basics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Julia offers multiple syntax for named function definition: (i) the basic one, which is similar to that of `Python`, but also (ii) a compact one, which is similar to function definition in mathematics."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-wrk_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/work-typing-computer-notebook-731198/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Basic definition: keywords \"function\", and \"end\"\n",
    "# Allowed functions name rule are the same for variables\n",
    "# The last value computed inside a function is its return value\n",
    "function Πυθαγόρας(a, b)\n",
    "    √(a*a + b*b)\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# And here is the function call\n",
    "Πυθαγόρας(12, 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compact (terse) definition:\n",
    "Pythagoras(a, b) = √(a*a + b*b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# And here is the function call\n",
    "Pythagoras(12, 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Either definition produces the same `function`. Compare:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@code_typed Πυθαγόρας(3.0, 4.0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@code_typed Pythagoras(3.0, 4.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Functions in Julia are first-class objects; so they can be assigned to variables:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = Πυθαγόρας"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f(3.0, 4.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Functions can be anonymous, just like in `lisp` and `Python` lambdas. In this case, basic and compact (terse) declarations become:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MODEL = Dict(\n",
    "    \"linear\" => function (x)\n",
    "            return x\n",
    "        end,\n",
    "    \"quadratic\" => x -> x*x,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MODEL[\"linear\"](5.0), MODEL[\"quadratic\"](5.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Function definitions can benefit from Julia `Compound Expressions`: either (i) `begin ... end`, or (ii) `(...; ...)`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# My Tuple of Functions:\n",
    "ToF = (\n",
    "    # First function\n",
    "    x -> (print(\"x = $x; \"); x),\n",
    "    # Second function\n",
    "    function (x) begin y = x*x; print(\"x² = $y; \"); y end end,\n",
    ");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calling one by one with the argument 3.0\n",
    "ToF[1](3.0), ToF[2](3.0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calling all with the argument 3.0 with an array comprehension and splatting:\n",
    "tuple([ i(3.0) for i in ToF ]...)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Julia, most operators are just functions with support for special syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The addition function: returns the sum of its arguments\n",
    "+(1, 2, 3, 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The multiplication function: returns the product of its arguments\n",
    "_4! = *(1, 2, 3, 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Julia one can assign and pass operators such as `+` and `*`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Assign the + operator to Σ:\n",
    "Σ = +\n",
    "Σ(1, 2, 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1 Defining Infix Operators\n",
    "\n",
    "OK... this is clearly not a basic thing! (But the context feels right)!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Julia allows some functions named with a single math symbol to be used (called) as infix operators. The list of allowed symbols (along with their precedence) is found in the first lines of the [Julia parser source code](https://github.com/JuliaLang/julia/blob/master/src/julia-parser.scm). The following is an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a \"∓\" function (this symbol is allowed to be used as an infix operator)\n",
    "# This will have the same precedence as an addition\n",
    "∓(x, y) = (x - y, x + y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use the function as an infix operator\n",
    "3 ∓ 1 * 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Or, call it like a normal function:\n",
    "∓(3, *(1, 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Operators with special names\n",
    "\n",
    "From the [Julia Documentation](https://docs.julialang.org/en/v1/manual/functions/#Operators-With-Special-Names-1), \"A few special expressions correspond to calls to functions with non-obvious names. These are:\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-bal_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/ball-http-www-crash-administrator-63527\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here our typeVal macro will come in handy!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "macro typeVal(x)\n",
    "    return quote              # The return value is an expression that is evaluated\n",
    "        local v = $x          # The value of x\n",
    "        local t = typeof(v)   # The type of the value\n",
    "        println(\"$(t): $(v)\") # Prints \"TYPE: VALUE\"\n",
    "        v                     # The value is returned\n",
    "    end\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1 Matrix ones: Concatenating and Adjoining"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Horizontal concatenation (i.e., a matrix, since vectors are columns)\n",
    "(  (@typeVal [1 2 3]), (@typeVal hcat(1, 2, 3))  )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Vertical concatenation (i.e., column vectors)\n",
    "(  (@typeVal [1; 2; 3]), (@typeVal vcat(1, 2, 3))  )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Concatenation in both directions (matrix concatenation)\n",
    "(  (@typeVal [1 2; 3 4]), (@typeVal hvcat((2, 2), 1, 2, 3, 4))  )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Adjoinin – Transposing\n",
    "A = [1 2; 3 4]\n",
    "display(A)\n",
    "display(A')\n",
    "display(adjoint(A))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2 Data Member ones: Indices and Properties"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set/Get index (column-based for matrices):\n",
    "A[2] = 9; setindex!(A, 9, 2);\n",
    "display( (A[2], getindex(A, 2)) )\n",
    "display(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to illustrate `getproperty()` and `setproperty!()`, let's create a 2D point `struct`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Data structure that holds two floating point coordinates:\n",
    "mutable struct Point2D\n",
    "    x::AbstractFloat\n",
    "    y::AbstractFloat\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Instance of the data structure object we've just created:\n",
    "here = Point2D(4.2, -1.8)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# getproperty:\n",
    "x = here.x\n",
    "y = getproperty(here, :y)  # Here, \":y\" is a Symbol for y\n",
    "print(\"The coordinates are $x and $(y)!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# setproperty:\n",
    "here.x = 2x\n",
    "setproperty!(here, :y, 2y)\n",
    "print(\"The new coordinates are $(here.x) and $(here.y)!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Tuples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In these presentations we heve been using `tuples` for quite a number of examples.\n",
    "\n",
    "Tuples are a immutable collection of items, and their literals are declared by a collection of items separated by commas, all between `()` parenthesis, which are mandatory only if the syntax is otherwise ambiguous:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-chn_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/chain-stainless-steel-metal-iron-3481377/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# A three-tuple build without parenthesis:\n",
    "@typeVal 1, 'a', 3.0;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Another with parenthesis:\n",
    "@typeVal (1, 2, 3);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Those that come to `Julia` from `Python` are likely to have knowledge of tuples. Just like in `Python`, Julia `tuple`s elements are accessible by integer indices (however starting at 1), and `tuple`s are _immutable_:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Attempt changing a tuple:\n",
    "a = (\"going\",)\n",
    "@typeVal a;\n",
    "try\n",
    "    a[1] = \"goiŋ\"\n",
    "finally\n",
    "    @typeVal a;\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.1 Named tuples\n",
    "\n",
    "In Julia, `tuple` elements can be named. Named elements are accessible by the `.:name` – recall that's syntax sugar for `getproperty()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Named tuple:\n",
    "coord = (x = 3.145, y = 2.718)\n",
    "print(\"The coordinates are $(coord.x) and $(getproperty(coord, :y))!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Julia `tuple`s allow for a number of useful function behavior:\n",
    "\n",
    "- Multiple return values;\n",
    "- Assign multiple variables at one (with one liners);\n",
    "- Argument destructuring (by tuple expansion);\n",
    "- Variable number of arguments;\n",
    "- Keyword arguments.\n",
    "\n",
    "The examples below illustrate these features:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Multilpe return values, and multiple assignments with one line\n",
    "lower, higher = 150 ∓ 10"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Argument destructuring: whenever function argument is a tuple, say, (x, y) ...\n",
    "# ... then an assignment (x, y) = argument is performed before entering the function.\n",
    "average((x, y)) = +(x, y) / 2.0\n",
    "println(average((lower, higher)))\n",
    "println(average(∓(150, 10)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Varargs function: Let's define a function that computes a series of subtractions:\n",
    "function ⊖(a, b, c...)  # Varargs declaration - variable arguments end up inside a tuple\n",
    "    if c == tuple()\n",
    "        return a - b\n",
    "    else\n",
    "        a - b - (+(c...))  # Here, c... is a tuple splatting\n",
    "    end\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The one-liner equivalent:\n",
    "⊟(a, b...) = b == tuple() ? a : a - (+(b...))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Computes: (i) 10-9; (ii) 10-5-4; (iii) 10-4-3-2-1\n",
    "display( (⊖(10, 9), ⊖(10, 5, 4), ⊖(10, 4, 3, 2, 1)) )\n",
    "display( (⊟(10, 9), ⊟(10, 5, 4), ⊟(10, 4, 3, 2, 1)) )\n",
    "display( (10 ⊟ 9, 10 ⊟ 5 ⊟ 4, 10 ⊟ 4 ⊟ 3 ⊟ 2 ⊟ 1) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Keyword arguments: appear after the semicolon\n",
    "# This function returns either a Dirac or a Kronecker delta multiple\n",
    "function δ(x; kind=\"Kronecker\")\n",
    "    if kind == \"Kronecker\"\n",
    "        return x .* [1 0; 0 1]  # The .* syntax is an element-by-element broadcast\n",
    "    elseif kind == \"Dirac\"\n",
    "        return x == 0 ? +Inf : zero(x)\n",
    "    else\n",
    "        nothing\n",
    "    end\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display( δ(3; kind=\"Dirac\") )\n",
    "display( δ(3; kind=\"Kronecker\") )\n",
    "display( δ(3; kind=\"Nonsense\") )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Do-Block Syntax"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some Julia functions, like `map()` can receive a function as one of its input arguments. When passing a function definition to those functions, the syntax can be cleaner if the function definition is placed inside a `do ... end` block:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-web_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/code-html-digital-coding-web-1076536/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Our test function - doubles positive numbers\n",
    "function doublePos(x)\n",
    "    if x >= 0\n",
    "        2x\n",
    "    else\n",
    "        x\n",
    "    end\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sample variable for our tests:\n",
    "A = δ(3; kind=\"Kronecker\") - [0 5; 5 0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Example use of map to apply the doublePos() function:\n",
    "map(doublePos, A)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# INLINE example use of map to apply an anonymous function:\n",
    "map(x -> begin\n",
    "        if x >= 0\n",
    "            2x\n",
    "        else\n",
    "            x\n",
    "        end\n",
    "    end, A)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Same INLINE example with a do-block: (More readable)\n",
    "map(A) do x\n",
    "    if x >= 0\n",
    "        2x\n",
    "    else\n",
    "        x\n",
    "    end\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Function Chaining"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Function chaining allows successive function applications in the arguments provided. The concept is similar to pipelining in a shell. In Julia, function chaining is facilitated with the `|>` function (which looks like a shell pipe):"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-cop_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/copper-fittings-plumbing-metal-1039483/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The |> function used as an infix operator merely applies the function over the previous operand:\n",
    "# Get the first 5 natural numbers squared:\n",
    "[1:5;] |> x -> x.*x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avg(x) = sum(x) / length(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Harmonic mean step-by-step:\n",
    "A = [1 2 4 8 16]\n",
    "A  |> display\n",
    "A .|> inv |> display\n",
    "A .|> inv |> avg |> display\n",
    "A .|> inv |> avg |> inv |> display"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Root mean square step-by-step:\n",
    "A = [1 2 4 8 16]\n",
    "A  |> display\n",
    "A  |> x -> x.*x |> display\n",
    "A  |> x -> x.*x |> avg |> display\n",
    "A  |> x -> x.*x |> avg |> sqrt |> display"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Julia also defines a composition operator `∘`, so that `f ∘ g` is the same as `f(g(...))`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "si = sqrt ∘ inv"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "si(3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "map(reverse ∘ uppercase, (\"Julia\",))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "using Unicode\n",
    "join(reverse(collect(graphemes(\"ax̂e\"))))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "map(join ∘ reverse ∘ collect ∘ graphemes, (\"ax̂e\",))[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Version Information"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"./fig/03-juliaREPL-versionInfo.png\" alt=\"Version Information\" width=\"892\">"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.0.0",
   "language": "julia",
   "name": "julia-1.0"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.0.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
