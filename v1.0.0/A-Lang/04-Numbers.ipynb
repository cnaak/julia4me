{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img alt=\"Julia Language Logo\" src=\"https://docs.julialang.org/en/v1/assets/logo.png\" width=\"96\" align=\"left\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Numbers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This material closely follows the [Julia Language Documentation](https://docs.julialang.org/en/v1/) for the `Version 1.0.0` of `julia`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-clf_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/calf-cow-maverick-farm-animal-farm-362170/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Engineering and scientific computing involves a fair amount of **number crunching**. Part of `julia` code speed comes from compiler optimizations, which are based on the assumptions it can make on the **types of data** in which the code operates.\n",
    "\n",
    "Although Julia does not impose explicit type declaration on the developer, **fast `julia` code** is partly achieved with **data type declarations**. These actually benefit not only runtime speeds, but also memory usage, through better allignment.\n",
    "\n",
    "Thus, it is important to begin with knowing numerical types."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Real numbers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1 Integers\n",
    "\n",
    "Julia has `8`, `16`, `32`, `64`, and `128` native bit integer types, all with `signed` and `unsigned` variants.\n",
    "\n",
    "The corresponding type names are: `Int8`, `UInt8`, `Int16`, `UInt16`, `Int32`, `UInt32`, `Int64`, `UInt64`, `Int128`, and `UInt128`.\n",
    "\n",
    "The default integer type is `Int32` in 32-bit systems, and `Int64` in 64-bit systems. `Sys.WORD_SIZE` returns the actual system word size:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-aba_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/abacus-calculus-classroom-count-1866497/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Check whether this system is a 32- or 64-bit one:\n",
    "Sys.WORD_SIZE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The default integer can be inferred with the `Int` and `UInt` types:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Check the default signed and unsigned integer sizes:\n",
    "Int, UInt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Therefore, unannotated integer literals are assumed as default integer types:\n",
    "typeof(1), typeof(-1000_000), typeof(+1000_000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that in `julia`, one can use _ as a visual separator of an integer literal anywhere. This is useful when transposing tabulated data with several significant figures onto a source code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Adding thousands (10³)\n",
    "x = 1_001 +\n",
    "    2_002 +\n",
    "    3_003"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Adding myriads (10⁴):\n",
    "y = 1_0001 +\n",
    "    2_0002 +\n",
    "    3_0003"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "According to the [Julia Documentation](https://docs.julialang.org/en/v1/), the minimum and maximum representable values of primitive numeric types such as integers are given by the `typemin` and `typemax` functions, hence:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Pretty-prints the integer types minimum and maximum values\n",
    "\n",
    "# Function to pretty-print — TYPE: [MIN, MAX]\n",
    "pp(T) = println(\"$(lpad(T,7)): [$(typemin(T)),$(typemax(T))]\")\n",
    "\n",
    "# Type arrays\n",
    "sIntTypes = [ Int8,  Int16,  Int32,  Int64,  Int128];   #   Signed Integer Types\n",
    "uIntTypes = [UInt8, UInt16, UInt32, UInt64, UInt128];   # Unsigned Integer Types\n",
    "\n",
    "# Do the pretty-printing\n",
    "println(\"-\" ^ 72)\n",
    "for T in sIntTypes pp(T) end\n",
    "println(\"-\" ^ 72)\n",
    "for T in uIntTypes pp(T) end\n",
    "println(\"-\" ^ 72)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.1.1 Integer literals\n",
    "\n",
    "Integer literals are ubiquitous in science and in engineering codes. **In general**, we want the type that will render the fastest computations, Julia does that to us by choosing integer types based on the processor architecture, as shown above, **provided that the number is within the type range**, evidently."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before proceeding, let's define a macro that prints the type of the argument and returns its value, as to better \"see\" what's happening:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# For now, think of macros as kind of functions that run on compile time, rather than at runtime\n",
    "macro typeVal(x)\n",
    "    return quote              # The return value is an expression that is evaluated\n",
    "        local v = $x          # The value of x\n",
    "        local t = typeof(v)   # The type of the value\n",
    "        println(\"$(t): $(v)\") # Prints \"TYPE: VALUE\"\n",
    "        v                     # The value is returned\n",
    "    end\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This macro can be used as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Macro executes (prints and returns the value)\n",
    "# The Julia REPL also prints the value:\n",
    "@typeVal 7"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since the macro returns the value, we can assign it to a variable (while still executing the macro):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Macro executes and the return value (of 7) is assigned to x\n",
    "# The Julia REPL also prints the value of x:\n",
    "x = @typeVal 7"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we want to suppress printing the output of the value by the Julia REPL, we end the statement with a semicolon:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Macro executes (prints and returns the value)\n",
    "# Now the Julia REPL keeps quiet (and no variable is assigned the value of 8)\n",
    "@typeVal 8;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A literal value that is too large for `Int64` will be stored in an `Int128` type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# A literal integer value of 10^20 is just too large for Int64:\n",
    "@typeVal 1_00000_00000_00000_00000;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes, especially when dealing with byte sequences, or other low-level integer operation, such as precise data layout, one typically wants to be able to represent a fix-sized numeric byte sequence, rather than just an integer value to be inserted into calculations. This is accomplished in `julia` by integer literals in different bases (binary, octal, or hexadecimal), as follows.\n",
    "\n",
    "In this case, `julia` always produces the smallest integer data type that is able to hold the value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal 0xFF\n",
    "@typeVal 0xFFFF\n",
    "@typeVal 0xFFFF_FFFF\n",
    "@typeVal 0xFFFF_FFFF_FFFF_FFFF\n",
    "@typeVal 0xFFFF_FFFF_FFFF_FFFF_FFFF_FFFF_FFFF_FFFF;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Other than these mechanisms, one can make **explicit conversions** to any type. Note, however, that extra processing is required to make the conversion, including sanity checks (whether the target type can express the value in an exact way). If your code heavily relies on these, it will be slower!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal Int8(@typeVal 32);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The explicit conversion triggers the generation of a lot of code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@code_typed Int8(32)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Which translates to a lot of machine code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@code_native Int8(32)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.1.2 Large integers\n",
    "\n",
    "There is another integer type called `BigInt` for **arbitrarily large** integers:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/640px-4LGSF_13.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://commons.wikimedia.org/wiki/File:4LGSF_13.jpg\">commons.wikimedia.org</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute 2 ^ 5.000 > 10 ^ 1505\n",
    "BigInt(2) ^ 5_000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.1.3 Numerical literal coefficients\n",
    "\n",
    "Julia permits preceeding variables by a numeric literal (just like in manual arithmetic) implying product. This mathematically familiar notation is particularly helpful for coefficients and polynomials.\n",
    "\n",
    "The Juila Documentation refers to this as \"Juxtaposed literal coefficient syntax\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Function that returns the (2x²+3x-6) polynomial evaluated at the given x:\n",
    "function p(x)\n",
    "    return 2x^2 + 3x - 6\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p(0), p(1), p(2), p(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But... beware of exceptions! Whenever the syntax introduces ambiguity, it is resolved in favor of interpretation as numeric literals.\n",
    "\n",
    "From the Julia Documentation:\n",
    "\n",
    "> - Expressions starting with `0x` are always hexadecimal literals.\n",
    "> - Expressions starting with a numeric literal followed by `e` or `E` are always floating-point literals.\n",
    "> - Expressions starting with a numeric literal followed by `f` are always 32-bit floating-point literals."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2 Floating point numbers\n",
    "\n",
    "Julia has `32` and `64` bit hardware floating point types, and a `16` bit software floating point types (that uses `32` bit floating point for calculations).\n",
    "\n",
    "The corresponding type names are: `Float32` and `Float64`, for the hardware types, and `Float16` for the software type.\n",
    "\n",
    "From the Julia Language Documentation, `Float64` \"literal floating-point numbers are represented in the standard formats, using E-notation when necessary\". Literal Float32 values can be entered by writing an `f` in place of `e`:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-pie_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/pi-board-school-district-diameter-1453836/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal 1.0, 0.1, 1e-1, 1f0, 1f-1;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal Float32(@typeVal 0.1);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Moreover, from the Julia Language Documentation, \"Hexadecimal floating-point literals are also valid, but only as `Float64` values, with `p` preceding the base-2 exponent\":"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal 0xFp3;  # (= 15 * 2^3)\n",
    "@typeVal 0x1p10; # (= 2 ^ 10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.2.1 Binary representation of a floating point number\n",
    "\n",
    "The exact bits of a floating point number can be recovered with `bitstring()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal bitstring(-2.0); @typeVal bitstring( 4.0); @typeVal bitstring( 4.4);\n",
    "@typeVal bitstring(-2f0); @typeVal bitstring( 4f0); @typeVal bitstring(4.4f0);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.2.2 Floating point zero(es)\n",
    "\n",
    "Floating point numbers have two zeros: a positive and a negative one. Both are treated as equal in calculations, but differ in binary representation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The == operator tests for \"general equality\"\n",
    "0.0 == -0.0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The === operator tests for indistinguishability\n",
    "0.0 === -0.0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "?=="
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "?==="
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal bitstring(+0.0); @typeVal bitstring(-0.0);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal bitstring(-1.0 * (-0.0)); @typeVal bitstring(+1.0 * (-0.0));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.2.3 Special floating point values\n",
    "\n",
    "According to the Julia Documentation, \"there are three specified standard floating-point values that do not correspond to any point on the real number line:\" `Nan`, `+Inf`, and `-Inf`, with the following properties:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "1.0/Inf, 1.0/0.0, -1.0/Inf, -1.0/0.0, 0.0/0.0, Inf+Inf, Inf-Inf, Inf/Inf, 0.0*Inf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "typemin(Float32), typemax(Float32), typemin(Float64),typemax(Float64)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.2.4 Floating point epsilon\n",
    "\n",
    "Although floating point numbers can represent a wide range of magnitudes, owing to the limited precision (finite number of mantissa bits), floating point additions (and subtractions) are especially prone to truncation erros, especially when the operands are of different magnitudes.\n",
    "\n",
    "The so-called `epsilon`, is the smallest number that can be added to a floating point number (usually the unity), that will make the result different than the operands.\n",
    "\n",
    "Julia provides the `eps()`, `nextfloat()`, and `prevfloat()` functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eps(Float32), eps(Float64), eps(1.0), eps(1.0e+20)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal bitstring(1.0e20); @typeVal bitstring(1.0 + 1.0e20);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "1.0 === 1.0 + eps(1.0) / 2.0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x32 = 1.0f0; x64 = 1.0e0;\n",
    "@typeVal nextfloat(x32); @typeVal nextfloat(x64);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal bitstring(x32); @typeVal bitstring(nextfloat(x32));"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nextfloat(x64) - x64, eps(x64)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.2.5 Large floats\n",
    "\n",
    "There is another floating point type called `BigFloat` for **arbitrary precision** floating point calculations. There are some parameters that control the behavior of `BigFloat`s, i.e., precision and rounding:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal setrounding(BigFloat, RoundUp) do\n",
    "    BigFloat(1) + parse(BigFloat, \"0.1\")\n",
    "end;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal setrounding(BigFloat, RoundDown) do\n",
    "    BigFloat(1) + parse(BigFloat, \"0.1\")\n",
    "end;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal begin\n",
    "    setrounding(BigFloat, RoundUp);\n",
    "    setprecision(400);\n",
    "    parse(BigFloat, \"1.1\")\n",
    "end;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Outside the `do ... end` and the `begin ... end` blocks, `BigFloat` precision remains the default 40 (bits):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "BigFloat.size"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Complex numbers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-man_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/mandelbrot-september-fractal-buddha-916463/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Complex numbers are formed using the `im` — the imaginary unit — for the imaginary part. Since `im` is a variable, juxtaposition (of coefficients) rules apply:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "?im"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal 1im; @typeVal 3 + 2im;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal 1.0im; @typeVal 3.0 + 2im;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Complex math works as expected:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Products\n",
    "(1 + im) * (1 - im), ((1 / √2) * (1 + im)) ^ 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Roots\n",
    "y, z = √Complex(-1.0), √√Complex(-1.0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Standard complex functions\n",
    "real(z), imag(z), conj(z), abs(z), abs2(2z), angle(z)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Rational numbers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Julia rational numbers represent fractions exactly, and are constructed with nominator and denominator separated by `//`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@typeVal begin\n",
    "    setrounding(BigFloat, RoundDown);\n",
    "    setprecision(400);\n",
    "    BigFloat(10//3)\n",
    "end;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Rational numbers behave exactly like manually computed fractions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change/simplify the denominator, and exact inverse:\n",
    "1//3 - 1//2, 1//3 + 2//3, inv(1//3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Version Information"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"./fig/03-juliaREPL-versionInfo.png\" alt=\"Version Information\" width=\"892\">"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.0.0",
   "language": "julia",
   "name": "julia-1.0"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.0.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
