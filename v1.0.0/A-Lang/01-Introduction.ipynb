{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img alt=\"Julia Language Logo\" src=\"https://docs.julialang.org/en/v1/assets/logo.png\" width=\"96\" align=\"left\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This material closely follows the [Julia Language Documentation](https://docs.julialang.org/en/v1/) for the `Version 1.0.0` of `julia`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-beg_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/road-asphalt-sky-clouds-fall-220058/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Why Julia?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1. Julia is fast\n",
    "\n",
    "Check the following benchmark, in which runtimes are **normalized with `C`** (times in `C` are all equal to unity). Also note the logarithmic runtime (vertical) scale. **Lower** runtime values means **faster**.\n",
    "\n",
    "<img src=\"https://static.squarespace.com/static/549dcda5e4b0a47d0ae1db1e/54a06d6ee4b0d158ed95f696/54a06d6fe4b0d158ed95ffd1/1409843895067/1000w/Julia_benchmarks.png\" alt=\"Julia Benchmarks\" width=\"640\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2. Julia is simple:\n",
    "\n",
    "Just like with `Python`, codes in `julia` are **shorter** if compared to `C`, `C++`, or `Fortran`. **Less keystrokes** generaly mean **faster development**.\n",
    "\n",
    "<img src=\"http://www.oceanographerschoice.com/log/wp-content/uploads/julia_benchmarks.png\" alt=\"Comparative code length\" width=\"640\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "...hence\n",
    "\n",
    "### 1.3. Julia solves the \"two-language\" problem:\n",
    "\n",
    "- Having one language for prototyping—usually a scripting language like `Python` that is **easy** to use, but **slow** ...\n",
    "- And another language for production—usually a lower-level, compiled one like `C` or `C++` that is **hard** to use, but **fast**.\n",
    "\n",
    "Since `julia` is **both easy and fast**, one can **prototype** code that **can** be **as fast as `C`**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "...but not only that, according to the [Julia Language](https://julialang.org) website:\n",
    "\n",
    "### 1.4. Julia is Dynamic, Optionally Typed, General, Technical, Composable... and more!\n",
    "\n",
    "- **Dynamic**: _Julia is dynamically-typed, feels like a scripting language, and has good support for interactive use._\n",
    "- **Optionally Typed**: _Julia has a rich language of descriptive datatypes_ (but does not force it on programmers).\n",
    "- **General**: _Julia uses multiple dispatch as a paradigm, making it easy to express many object-oriented and functional programming patterns._\n",
    "- **Technical**: _Julia excels at numerical computing. Its syntax is great for math, many numeric datatypes are supported, and parallelism is available out of the box._\n",
    "- **Composable**: _Julia packages naturally work well together._ Moreover, `julia` can call `C` and `Python` code!\n",
    "- **and more**: `julia` also supports metaprogramming—programs that write programs!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. A Taste of Julia"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-cup_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/teacup-coffee-grains-caffeine-3637705/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1 Fibonacci number generator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**A.** Function definition using basic syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function Fib1(n)\n",
    "    if n < 2\n",
    "        return n\n",
    "    else\n",
    "        return Fib1(n-1) + Fib1(n-2)\n",
    "    end\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tuple((Fib1(n) for n in 1:20)...)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**B.** Function definition using compact (assignment form) syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Fib2(n) = n < 2 ? n : Fib2(n-1) + Fib2(n-2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tuple((Fib2(n) for n in 1:20)...)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2 Unicode variables and functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function Δ(a, b)\n",
    "    return b - a\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "þ, Я, 光 = 1, 2, 3\n",
    "Δ(þ, Я)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Δ(0, 光)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3 Code inspection up to machine instructions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@code_lowered Δ(3, 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@code_typed Δ(3, 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@code_llvm Δ(3, 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "code_native(Δ, (Int64, Int64))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Few machine instructions means the code runs really fast!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.4 Code benchmarking:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "using BenchmarkTools"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@benchmark Δ(3, 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Version Information"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"./fig/03-juliaREPL-versionInfo.png\" alt=\"Version Information\" width=\"892\">"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.0.0",
   "language": "julia",
   "name": "julia-1.0"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.0.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
