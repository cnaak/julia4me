{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img alt=\"Julia Language Logo\" src=\"https://docs.julialang.org/en/v1/assets/logo.png\" width=\"96\" align=\"left\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Variables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This material closely follows the [Julia Language Documentation](https://docs.julialang.org/en/v1/) for the `Version 1.0.0` of `julia`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-var_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/geometry-mathematics-volume-surface-1044090/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Variables: assignment, reassignment, and use"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A variable, in Julia, is a name associated (or bound) to a value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Variable assignment\n",
    "a = 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculations with variables\n",
    "# and variable reassignment\n",
    "a = √(a + 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since Julia is a dynamic language, variables have no (fixed) types, but (their) values do."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = √3\n",
    "println(typeof(a))\n",
    "a = 2\n",
    "println(typeof(a))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Variables: allowed names"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table align=\"right\">\n",
    "  <tr>\n",
    "    <td>\n",
    "        <img src=\"./fig/CC0-rib_640.jpg\" width=\"480\">\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>\n",
    "        Source:\n",
    "        <a href=\"https://pixabay.com/en/wordpress-lanyards-blog-blogging-552922/\">pixabay.com</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the [Julia Language Documentation](https://docs.julialang.org/en/v1/):\n",
    "\n",
    "> Variable names must begin with a letter (A-Z or a-z), underscore, or a subset of Unicode code points greater than 00A0; in particular, [Unicode character categories](http://www.fileformat.info/info/unicode/category/index.htm) Lu/Ll/Lt/Lm/Lo/Nl (letters), Sc/So (currency and other symbols), and a few other letter-like characters (e.g. a subset of the Sm math symbols) are allowed. Subsequent characters may also include ! and digits (0-9 and other characters in categories Nd/No), as well as other Unicode code points: diacritics and other modifying marks (categories Mn/Mc/Me/Sk), some punctuation connectors (category Pc), primes, and a few other characters.\n",
    "\n",
    "This means that options are abundant, and the following are valid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This variable uses partial derivative signs (∂: U+2202)\n",
    "∂y∂x = 8    # This doesn't work in Python3!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Latin Suplements and Extensions (Old English)\n",
    "þ, ſ, ð, ʒ, ŋ = \"thorn: U+FE\", \"long s: U+17F\", \"eth: U+F0\", \"ezh: U+292\", \"eng: U+14B\"\n",
    "@show þ, ſ, ð, ʒ, ŋ"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Greek and Coptic: TAB-completes with LaTeX syntax: \\alpha, \\beta, etc...\n",
    "α, β, γ = 1, 2, 3\n",
    "@show α, β, γ"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Cyrillic\n",
    "Чайковский = \"Tchaikovsky\";\n",
    "Ни́жний_Но́вгород = \"Nizhny Novgorod\";\n",
    "@show (Чайковский, Ни́жний_Но́вгород)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Hiragana\n",
    "つなみ = \"Tsunami\"\n",
    "@show つなみ"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# CJK Unified Ideographs\n",
    "中国 = \"China\"\n",
    "@show 中国"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Version Information"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"./fig/03-juliaREPL-versionInfo.png\" alt=\"Version Information\" width=\"892\">"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.0.0",
   "language": "julia",
   "name": "julia-1.0"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.0.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
