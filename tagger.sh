#!/bin/bash

tag="$(glog | cut -f1,2 -d-)"

cat << EOF
git tag -a "rel-${tag}" -m "Release ${tag}"
EOF

