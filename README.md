# julia4me

Julia language introduction for (my) Mechanical Engineering classes

## Brief

This project begins as a personal, however open, set of presentation materials in the  topic  of
[the Julia Language](https://julialang.org), originally put together as support for  my  classes
in Mechanical Engineering.

The         materials         closely         follow         the         [Julia         Language
Documentation](https://docs.julialang.org/en/v1/).

## Author

Prof. Christian Naaktgeboren, PhD [(CV Lattes)](http://lattes.cnpq.br/8621139258082919)

Federal University of Technology, Paraná [(site)](http://portal.utfpr.edu.br), Guarapuava Campus
[(site)](http://portal.utfpr.edu.br/campus/guarapuava)

[Research Center for Thermal Sciences](http://dgp.cnpq.br/dgp/espelhogrupo/1908587338699361)

Guarapuava, PR, Brazil

NaaktgeborenC <dot!> PhD {at!} gmail [dot!] com

## License

This project is [licensed](https://gitlab.com/cnaak/julia4me/blob/master/LICENSE) under the  MIT
license.

## Citations

How to cite this project:

```bibtex
@Misc{2018-NaaktgeborenC-julia4me,
  author       = {C. Naaktgeboren},
  title        = {{julia4me} -- Julia language introduction for (my) Mechanical Engineering classes},
  howpublished = {Online},
  year         = {2018},
  journal      = {GitLab repository},
  publisher    = {GitLab},
  url          = {https://gitlab.com/cnaak/julia4me},
}
```

